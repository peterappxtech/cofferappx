<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});
Route::get('/home_page', function () {
    return view('pages.home');
});
Route::get('/signup_user', function () {
    return view('pages.sign_up_user');
});
Route::get('/myaccount', function () {
    return view('pages.myaccount');
});
Route::get('/property', function () {
    return view('pages.property');
});

Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/services', function () {
    return view('pages.services');
});

Route::get('/contact', function () {
    return view('pages.contact');
});
Route::get('/profile', function () {
    return view('pages.profile');
});

Route::group(['prefix' => 'user', 'middleware' => ['auth'], 'as' => 'user'], function() {

    Route::get('/{page}','UserPageController@index')->name('index');

    Route::get('/account', function () {
        return view('users.account');
    });
    
    Route::get('/maps', function () {
        return view('users.maps');
    });
    
    Route::get('/notifications', function () {
        return view('users.notifications');
    });
    
    Route::get('/profile', function () {
        return view('users.profile');
    });
    
    Route::get('/table', function () {
        return view('users.table');
    });
    
    Route::get('/templates', function () {
        return view('users.templates');
    });
    
    Route::get('/typography', function () {
        return view('users.typography');
    });
    
    Route::get('/upgrade', function () {
        return view('users.upgrade');
    });


});
Route::get('pdfview',array('as'=>'pdfview','uses'=>'logincontroller@pdfview'));

Auth::routes();

Route::get('/mylogin', function () {
    return view('pages.login');
});


Route::get('/signup', function () {
    return view('pages.signup');
});

Route::get('/forgot', function () {
    return view('pages.forgot');
});


Route::get('/404', function () {
    return view('pages.404');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::post('/login','UserLoginController@postLogin')->name('login');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/login_user', 'logincontroller@login')->name('login_user');
Route::get('logout', 'logincontroller@logout')->name('logout');
Route::post('profile_add', 'logincontroller@profile_add')->name('profile_add');

