<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserPageController extends Controller
{
    //

    public function _construct(){

       /* return view()->first(
            ['custom-template', 'default-template'], $data
        );
        */

        if (!Auth::check()) return view("errors.403");
    }

    public function index($page,Request $request){

        //return view("errors.403");

        $data = array();

        //abort(403);

        return view()->first(
            ["users.{$page}", 'errors.404'], $data
        );

    }
}
