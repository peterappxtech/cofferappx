<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class PhotoController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

  public function login(Request $request)
  {
          
        $rules = array(
            'email'    => 'required|email',
            'password' => 'required|alphaNum|min:3' 
        );
        
        $validator = Validator::make(Input::all(), $rules);
      
        if ($validator->fails()) {
            return Redirect::to('home')
                ->withErrors($validator) 
                ->withInput(Input::except('password')); // send back the input (
        } else {
            // create our user data for the authentication
            $userdata = array(
                'email'     => Input::get('email'),
                'password'  => Input::get('password')
            );
            // attempt to do the login
            if (Auth::attempt($userdata)) {
              
                 return Redirect::to('home');
            } else {
                // validation not successful, send back to form
                echo 'not login';
            }
        }
  }
}
