<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;
use DB;
use PDF;

class logincontroller extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    public function login(Request $request)
    {

        $rules = array(
            'email'    => 'required|email',
            'password' => 'required|alphaNum|min:3' 
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('')
            ->withErrors($validator) 
                ->withInput(Input::except('password')); // send back the input (
            } else {
            // create our user data for the authentication
                $userdata = array(
                    'email'     => Input::get('email'),
                    'password'  => Input::get('password')
                );

            // attempt to do the login
                if (Auth::attempt($userdata)) {
                  Session::put('email',$request->email,'id',$request->id);

                  return Redirect::to('')->with('message', 'Your Login Successfully!');
              } else {
                // validation not successful, send back to form
                return Redirect::to('')->with('message', 'Invalid Email id and Password!');
            }
        }
    }
    public function logout(Request $request) 
    {
      Auth::logout();
      Session::flush();
      return redirect('');
    }
    public function profile_add(Request $request)
    {
         $this->validate($request, [
        'bank_statement' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:5000',
                                  ]);
          $string = str_random(15);
       $getimageName = 'bank_statement'.$string.'.'.$request->bank_statement->getClientOriginalExtension();
      $request->bank_statement->move(public_path('images/userDocument'), $getimageName);
      $this->validate($request, [
        'salary_slip' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:5000',
                                  ]);
      $string = str_random(15);
       $getimage = 'salary_slip'.$string.'.'.$request->salary_slip->getClientOriginalExtension();
      $request->salary_slip->move(public_path('images/userDocument'),$getimage);
     $data=['user_id'=>1,
            'name'=>'sachin',
            'unique_id_number'=>$request->Unique_id_number,
            'income'=>$request->income,
            'bank_statement'=>$getimageName,
            'salary_slip'=>$getimage
             ];
             DB::table('profile')->insert($data);
             return Redirect::to('profile')->with('message', 'Your Profile Add Successfully!');
    
    }
    public function pdfview(Request $request)
    {
        $items = DB::table("profile")->get();
        view()->share('profile');


        if($request->has('download')){
            $pdf = PDF::loadView('pdfview', ['items' => $items]);
            return $pdf->download('pdfview.pdf');
        }


        return view('pdfview')->with('items',$items);
    }

}
