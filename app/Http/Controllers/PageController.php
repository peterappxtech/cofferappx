<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //

    public function _construct(){

       /* return view()->first(
            ['custom-template', 'default-template'], $data
        );
        */
    }

    public function index($page,Request $request){

        //dd($page);

        return view("users.{$page}");

    }
}
