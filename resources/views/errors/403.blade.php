@extends('layouts.errors')

@section('title', 'Login required')

@section('message', 'Sorry, you need to be logged in to view this page.')
