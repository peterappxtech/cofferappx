@extends('layouts.site')

@section('content')

<section id="forgot" class="section">

<div class="container">

        <div id="signupbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title">Forgot password</div>
                        </div>  
                        <div class="panel-body" >
                            <form id="signupform" class="form-horizontal" role="form">
                                 
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" placeholder="Email Address">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-retrieve" type="button" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Retrieve</button>
                                    </div>
                                </div>                                
                                
                            </form>
                         </div>
                    </div>
                
         </div> 
</div>

</section>