@extends('layouts.site')

@section('content')

<section id="hero-area">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <div class="block">
	                        <h1 class="wow fadeInDown">Helping your credit and you.</h1>
	                        <p class="wow fadeInDown" data-wow-delay="0.3s">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
	                        <div class="wow fadeInDown" data-wow-delay="0.3s">
	                        	<a class="btn btn-default btn-home" href="#about" role="button">Get Started</a>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-6 wow zoomIn">
	                    <div class="block">
	       
        <div id="signupbox">
                <div class="card">
        <form  method="POST" action="{{ route('register') }}">

                                <div class="header">Register</div>
                                <div class="content">

                                    <div class="form-group">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label">Email Address <star>*</star></label>
                                       
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                                    </div>
                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                     <div class="form-group">
                                         <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="control-label">name <star>*</star></label>
                                      
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                           </div>
                                    </div>

                                    <div class="form-group">
                                         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label">Password <star>*</star></label>
                                       
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                                    </div>
                                           <div class="form-group">
                             <label class="control-label">Confirm Password <star>*</star></label>

                           
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                           
                        </div>
                        <div class="form-group">
                             <label class="control-label">Role <star>*</star></label>
                        <select class="form-control" id="sel1" name="role">
                                <option>Select...</option> 
                                <option value="2">User</option>
                                <option value="3">Owner</option>
                                <option value="4">Property Manager</option>
                                <option value="5">Renter</option>
                             
                              </select>
                           
                        </div>

                                    

                                    <div class="category"><star>*</star> Required fields</div>
                                </div>

                                <div class="footer">
                                    <button type="submit" class="btn btn-info btn-fill pull-right">Register</button>
                                    <div class="form-group pull-left">
                                        <label class="checkbox">
                                            <span class="icons"><span class="first-icon fa fa-square-o"></span><span class="second-icon fa fa-check-square-o"></span></span><input type="checkbox" data-toggle="checkbox" value="subscribe">
                                            Subscribe to newsletter
                                        </label>
                                    </div>
                                    <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                            Have an account! 
                                        <a href="\">
                                            Sign In Here
                                        </a>
                                        </div>
                                    </div>
                                </div>

                                    <div class="clearfix"></div>
                                </div>
                                
                            </form>
                        </div>
         </div> 
	                    </div>
	                </div>
	            </div><!-- .row close -->
	        </div><!-- .container close -->
	    </section>
        @endsection