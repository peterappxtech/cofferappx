@extends('layouts.site')

@section('content')
<style type="text/css">
  .btn_custom a{
    border: 1px solid;
    padding: 6px;
    background-color: #91c96c;
    color: #fff;
  }
  .btn_custom{
        float: right;
  }
</style>
  </br></br> </br></br></br></br> 
<div class="container">
	        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12    toppad" >
   
   
          <div class="panel panel-info">
            <div class="panel-heading">
             
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <!-- <img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive">  --></div>
               
                <div class=" col-md-5 col-lg-5 "> 
                
                  <table class="table table-user-information">
                    <tbody>
                    	 <h3 class="panel-title">Sheena Shrestha</h3>
                      <tr>
                        <td>Department:</td>
                        <td>Programming</td>
                      </tr>
                      <tr>
                        <td>Hire date:</td>
                        <td>06/23/2013</td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td>01/24/1988</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Gender</td>
                        <td>Female</td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td>Kathmandu,Nepal</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:info@support.com">info@support.com</a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td>123-4567-890(Landline)<br><br>555-4567-890(Mobile)
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                
                </div>
                 <div class="col-md-4 ">
                 	<div class="btn_custom Pull-right">
        	
        	<a href="{{ route('pdfview',['download'=>'pdf']) }}" ><i class="fa fa-download" aria-hidden="true"></i> Download PDF</a>
        	

        </div>
        </div>
              </div>
            </div>
                
          </div>
        </div>
       
      </div>
    </div>

    @endsection

