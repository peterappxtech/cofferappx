@extends('layouts.site')

@section('content')

<section id="hero-area">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-6">
	                    <div class="block">
	                        <h1 class="wow fadeInDown">Helping your credit and you.</h1>
	                        <p class="wow fadeInDown" data-wow-delay="0.3s">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
	                        <div class="wow fadeInDown" data-wow-delay="0.3s">
	                        	<a class="btn btn-default btn-home" href="#about" role="button">Get Started</a>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-6 wow zoomIn">
	                    <div class="block">
	        <div id="loginbox">                    
            <div class="card">
                            <div class="header">Profile Page</div>

                            <div class="content">
                                 @if(Session::has('message'))
                            <p class="alert alert-info">{{ Session::get('message') }}</p>
                            @endif
                                <form class="form-horizontal" method="POST" action="\profile_add" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Unique Id Number</label>
                                        <div class="col-md-9">
                                            <input type="text" placeholder="Unique Id Number" class="form-control" name="Unique_id_number" required>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">income</label>
                                        <div class="col-md-9">
                                            <input type="text" placeholder="income" class="form-control" name="income">
                                        </div>
                                    </div> 
                                     <div class="form-group">
                                        <label class="col-md-3 control-label">Bank Statement</label>
                                        <div class="col-md-9">
                                            <input type="file"  class="form-control" name="bank_statement">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">salary slip</label>
                                        <div class="col-md-9">
                                            <input type="file"  class="form-control" name="salary_slip">
                                        </div>
                                    </div> 
                                  

                                    <div class="form-group">
                                        <label class="col-md-3"></label>
                                        <div class="col-md-9">
                                            <button type="submit" class="btn btn-fill btn-info pull-right">Update</button>
                                        </div>
                                    </div>
                                
                                </form>
                            </div>
                        </div>
        </div>
        
	                    </div>
	                </div>
	            </div><!-- .row close -->
	        </div><!-- .container close -->
	    </section>
        @endsection