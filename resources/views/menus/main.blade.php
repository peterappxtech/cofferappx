<ul class="nav navbar-nav navbar-right">
    @foreach($items as $menu_item)
        <li class="{{ Request::is(substr($menu_item->url,1)) ? 'current' : (Request::is($menu_item->url) ? 'current' : null) }}"><a href="{{ $menu_item->url }}">{{ $menu_item->title }}</a></li>
    @endforeach
</ul>

