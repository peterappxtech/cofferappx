<!doctype html>
<html lang="en">
<head>
@include('layouts.partials.dashboard._header')
</head>
<body>

    <div class="wrapper">

        @include('layouts.partials.dashboard._sidebar')

        <div class="main-panel">

            @include('layouts.partials.dashboard._navigation')

            @yield('content')

            @include('layouts.partials.dashboard._page_footer')

        </div>
    </div>


</body>

@include('layouts.partials.dashboard._footer')

</html>