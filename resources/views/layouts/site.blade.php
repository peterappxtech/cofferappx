<!doctype html>
<html lang="en">

<head>

	@include('layouts.partials.site._header')

</head>

<body> 

@include('layouts.partials.site._page_header')

<div class="wrapper wrapper-full-page">
    
        @yield('content')                           
       
</div>


</body>
    	
@include('layouts.partials.site._footer')
    
</html>