<footer class="footer footer-transparent">
            <div class="container">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="/">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="/about">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="/services">
                                Services
                            </a>
                        </li>
                        <li>
                            <a href="/contact">
                               Contact
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; 2017 <a href="http://mycredvault.com">My Cred Vault</a>
                </p>
            </div>
        </footer>