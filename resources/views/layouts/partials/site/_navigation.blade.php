<div class="navbar-default navbar-fixed-top animated" id="navigation">
	        <div class="container">
	            <!-- Brand and toggle get grouped for better mobile display -->
	            <div class="navbar-header">
	                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="<?php echo "home_page";?>">
	                    <img class="logo-2" src="images/logo-2.png" alt="LOGO">
	                </a>
	            </div>

	            <!-- Collect the nav links, forms, and other content for toggling -->
	              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
     
		        <li><a href="<?php echo URL::to('/').'/home_page';?>" class="page-scroll">Home</a></li>
		        <li><a href="<?php echo URL::to('/').'/about';?>" class="page-scroll">About</a></li>
		      <li><a href="<?php echo "services";?>" class="page-scroll">Services</a></li>
		        <li><a href="<?php echo "contact";?>" class="page-scroll">Contact</a></li>
		         <li><a href="<?php echo URL::to('/').'/property';?>" class="page-scroll">Property</a></li>
		         <?php  $val=Session::get('email');
		         if(isset($val)){

		          ?>
		         <li><a href="<?php echo "profile";?>" class="page-scroll">Profile</a></li>
		         <li><a href="<?php echo "logout";?>" class="page-scroll">Logout
		         </a></li>
		         <li><a href="<?php echo "myaccount";?>" class="page-scroll">My Account
		         </a></li>
		         <?php } else {?>

		        <li><a href="<?php echo "signup";?>" class="page-scroll">Sign Up</a></li>
		        <?php } ?>
		      </ul>
		    </div><!-- /.navbar-collapse -->
			        </div><!-- /.container-fluid -->
	    </div>
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	    <script>
        function myFunction() {
            var printButton = document.getElementById("download");             
            printButton.style.visibility = 'hidden';            
            window.print();    
            printButton.style.visibility = 'visible';
        }
</script>
