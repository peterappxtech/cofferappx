<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">    
        @include('layouts.partials.site._navigation')
    </div>
</nav>